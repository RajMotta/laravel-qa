<?php

use App\Models\User;

trait Votable {
    public function vote($vote) {
        $this->votes()->attach(auth()->id(), ['vote' => $vote]);
        if($vote > 0){
            $this->increment('votes_count');
        }   else    {
            $this->decrement('votes_count');
        }
    }

    public function updateVote(int $vote) {
        $this->votes()->updateExistingPivot(auth()->id(), ['vote' => $vote]);

        if($vote > 0){
            $this->increment('votes_count');
            $this->increment('votes_count');
        }   else    {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        }
    }

    public function deleteVote(int $vote) {
        $this->votes()->detach(auth()->id());
        if($vote > 0){
            $this->decrement('votes_count');
        }   else    {
            $this->increment('votes_count');
        }
    }

    public function hasMarkedUpvote(User $user) {
        return $this->votes()->where('user_id', $user->id)->where('vote', 1)->exists();
    }

    public function hasMarkedDownvote(User $user) {
        return $this->votes()->where('user_id', $user->id)->where('vote', -1)->exists();
    }

    public function hasVoted(User $user){
        return $this->votes()->where('user_id', $user->id)->exists();
    }
}
