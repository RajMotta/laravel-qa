<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'bio',
        'image'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Accessors
     */

    public function getAvatarAttribute(){
        if($this->image !== null) {
            return asset("storage/$this->image");
        }
        $name = $this->name;
        return "https://ui-avatars.com/api/?size=40&rounded=true&name={$name}";
    }

    public function getCreatedDateAttribute() {
        return $this->created_at->diffForHumans();
    }

    /**
     * Relationship Methods
     */

    public function questions(){
        return $this->hasMany(Question::class);
    }

    public function answers() {
        return $this->hasMany(Answer::class);
    }

    public function favourites() {
        return $this->belongsToMany(Question::class)->withTimestamps();
    }

    public function voteQuestions() {
        return $this->morphedByMany(Question::class, 'vote')->withTimestamps();
    }

    public function voteAnswers() {
        return $this->morphedByMany(Answer::class, 'votes')->withTimestamps();
    }
}
