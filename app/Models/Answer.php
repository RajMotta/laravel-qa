<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Votable;

require_once('traits/Votable.trait.php');

class Answer extends Model
{
    use HasFactory, Votable;

    protected $guarded = [];

    public static function boot() {
        parent::boot();
        static::created(function($answer) {
            $answer->question->increment('answers_count');
        });
    }

    public function getIsModifiedAttribute() {
        return ($this->modified_at);
    }

    public function getCreatedDateAttribute() {
        return $this->created_at->diffForHumans();
    }

    public function getModifiedDateAttribute() {
        $date = new Carbon($this->modified_at);
        return $date->diffForHumans();
    }

    public function getUrlAttribute(){
        return "{$this->question->url}#{$this->question->id}-{$this->id}";
    }

    public function isBest(Question $question){
        return $question->best_answer_id === $this->id;
    }

    public function question() {
        return $this->belongsTo(Question::class);
    }

    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function votes() {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }
}
