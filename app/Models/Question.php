<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;
use Symfony\Component\VarDumper\VarDumper;
use Votable;

require_once('traits/Votable.trait.php');

class Question extends Model
{
    protected $guarded = [];

    use HasFactory, Votable;

    public function markAsBest(Answer $answer)
    {
        $this->best_answer_id = $answer->id;
        $this->save();
    }

    public function unMarkAsBest() {
        $this->best_answer_id = null;
        $this->save();
    }

    /**
     * Mutators
     */

    public function setTitleAttribute($title){
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }

    /**
     * Accessor
     */

    public function getUrlAttribute() {
        return "questions/$this->slug";
    }

    public function getIsModifiedAttribute() {
        return ($this->modified_at);
    }

    public function getCreatedDateAttribute() {
        return $this->created_at->diffForHumans();
    }

    public function getModifiedDateAttribute() {
        $date = new Carbon($this->modified_at);
        return $date->diffForHumans();
    }

    public function getStylesForAnswerAttribute(){
        if($this->answers_count > 0){
            if($this->best_answer_id){
                return "has-best-answer";
            }   else    {
                return "answered";
            }
        }
        return "unanswered";
    }

    public function getIsFavouriteAttribute() {
        return $this->favourites()->where(["user_id" => auth()->id()])->count() > 0;
    }

    public function getFavouritesCountAttribute() {
        return $this->favourites->count();
    }

    public function getAnswersWithFavouriteAttribute() {
        $answers = array();
        foreach($this->answers as $answer) {
            if($answer->isBest($this)) {
                array_unshift($answers, $answer);
            }   else{
                array_push($answers, $answer);
            }
        }
        return $answers;
    }

    public function getTagsCountAttribute() {
        return $this->tags()->count();
    }

    public function hasThisTag(Tag $tag) {
        return $this->tags()->where("tag_id", $tag->id)->get()->count() == 1;
    }

    /**
     * Relationship Methods
     */

    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function answers() {
        return $this->hasMany(Answer::class);
    }

    public function favourites() {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function votes() {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

    public function tags() {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }
}
