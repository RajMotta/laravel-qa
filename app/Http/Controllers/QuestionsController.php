<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Models\Question;
use App\Models\Tag;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;

class QuestionsController extends Controller
{
    function __construct() {
        $this->middleware(['auth'])->except(["index", "show"]);
    }

    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10);
        session()->flash('location', "questions");
        return view('questions.index', compact(['questions']));
    }

    public function create()
    {
        session()->flash('location', 'ask_question');
        $tags = Tag::all();
        return view('questions.create', compact(['tags']));
    }

    public function store(CreateQuestionRequest $request)
    {
        $question = auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        $question->tags()->attach($request->tags);

        session()->flash('success', "Question has been reported to all expert users!");
        return redirect(route('questions.index'));
    }

    public function show(Question $question)
    {
        if(! isset($_COOKIE["$question->slug"])){
            setcookie($question->slug, true);
            $question->increment('views_count');
        }
        session()->flash('location', "questions");
        return view('questions.show', compact(['question']));
    }

    public function downloadAsPDF(Question $question) {
        set_time_limit(300);
        $pdf = Pdf::loadView('questions.show', compact(['question']));
        return $pdf->download("$question->slug.pdf");
    }

    public function edit(Question $question)
    {
        if($this->authorize('update', $question)){
            $tags = Tag::all();
            return view('questions.edit', compact(['question', 'tags']));
        }
        return abort(403);
    }

    public function update(UpdateQuestionRequest $request, Question $question)
    {
        if($this->authorize('update', $question)){
            $question->update([
                'title' => $request->title,
                'body' => $request->body,
                'modified_at' => Carbon::now()
            ]);

            $question->tags()->sync($request->tags);

            session()->flash('success', "Question has been updated and will be answered accordingly soon!");
            return redirect(route('questions.index'));
        }
        return abort(403);
    }

    public function destroy(Question $question)
    {
        if($this->authorize('delete', $question)) {
            $question->delete();
            session()->flash('success', "Question has been deleted!");
            return redirect(route('questions.index'));
        }
        return abort(403);
    }
}
