<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Notifications\QuestionMarkedAsFavourite;
use Illuminate\Http\Request;

class FavouritesController extends Controller
{
    public function store(Question $question) {
        $question->favourites()->attach(auth()->id());
        $question->owner->notify(new QuestionMarkedAsFavourite($question));
        return redirect()->back();
    }

    public function destroy(Question $question){
        $question->favourites()->detach(auth()->id());
        return redirect()->back();
    }
}
