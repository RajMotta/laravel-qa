<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function notifications() {
        $notifications = auth()->user()->unreadNotifications()->paginate(25);
        session()->flash('location', 'notifications');
        return view('users.notifications', compact(['notifications']));
    }

    public function markAsRead($notification) {
        $notification = auth()->user()->notifications()->find($notification);
        $notification->markAsRead();
        return redirect()->back();
    }

    public function markAllAsRead() {
        auth()->user()->unreadNotifications->markAsRead();
        return redirect()->back();
    }

    public function index() {
        $users = User::paginate(12);
        session()->flash('location', "users");
        return view('users.index', compact(['users']));
    }

    public function fetchAll() {
        $users = User::all();
        return response(["data" => $users, "code" => 200], 200);
    }

    public function show(User $user){
        session()->flash('location', "users");
        $user = User::with([
            'questions' => function($query) { $query->latest(); },
            'answers' => function($query) { $query->latest(); }
            ])->find($user->id);
        $userQuestions = $user->questions;
        $votes['questions'] = 0;
        $views = 0;
        foreach($userQuestions as $question){
            $votes['questions'] += $question->votes_count;
            $views += $question->views_count;
        }
        $userAnswers = $user->answers;
        $votes['answers'] = 0;
        foreach($userAnswers as $answer){
            $votes['answers'] += $answer->votes_count;
        }
        $usedTags = Question::with("tags")->where("user_id", $user->id)->get()->pluck("tags")->flatten();
        $tags = [];
        foreach($usedTags as $tag) {
            if(isset($tags[$tag->id])) {
                $tags[$tag->id]["count"] += 1;
            } else {
                $tags[$tag->id] = $tag;
                $tags[$tag->id]["count"] = 1;
            }
        }
        return view('users.profile', compact(['user', 'votes', 'views', 'tags']));
    }

    public function edit(User $user) {
        session()->flash('location', "users");
        return view('users.edit', compact(['user']));
    }

    public function update(Request $request, User $user) {
        if($request->file('image')){
            $image = $request->file('image')->store('user-images');
            $user->update([
                'image' => $image
            ]);
        }
        $user->update([
            'bio' => $request->bio,
            'name' => $request->name,
        ]);

        return redirect(route('users.profile', $user));
    }

    public function viewFavourites(User $user) {
        $questions = $user->favourites()->paginate(10);
        return view("users.favourites", compact(['questions', 'user']));
    }
}
