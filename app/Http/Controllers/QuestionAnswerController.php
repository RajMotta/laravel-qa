<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use App\Notifications\AnswerMarkedAsBest;
use App\Notifications\NewReplyAdded;
use App\Notifications\UserMentionedInAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class QuestionAnswerController extends Controller
{
    public function markAsBest(Question $question, Answer $answer){
        $this->authorize('markAsBest', $answer);
        $question->markAsBest($answer);
        $answer->author->notify(new AnswerMarkedAsBest($answer));
        session()->flash('location', "questions");
        return redirect()->back();
    }

    public function unMarkAsBest(Question $question, Answer $answer) {
        $this->authorize('unMarkAsBest', $answer);
        $question->unMarkAsBest();
        session()->flash('location', "questions");
        return redirect()->back();
    }

    public function create(Question $question){
        session()->flash('location', "questions");
        return view('answers.create', compact(['question']));
    }

    public function store(Request $request, Question $question){
        $answer = Answer::create([
            'user_id' => auth()->id(),
            'question_id' => $question->id,
            'body' => $request->body
        ]);

        $pattern = '/<a\shref=".*?">@(.*?)<\/a>/';
        $matches = [];
        preg_match_all($pattern, $request->body, $matches);

        $users = $matches[1];
        foreach($users as $user) {
            if(User::where('name', 'LIKE', $user)->count() > 0){
                $mentionedUser = User::where('name', 'LIKE', $user)->get()[0];
                $mentionedUser->notify(new UserMentionedInAnswer($answer));
            }
        }

        $question->owner->notify(new NewReplyAdded($answer));


        session()->flash('success', "Answer has been posted!");

        return redirect("/questions/{$question->slug}");
    }

    public function edit(Question $question, Answer $answer) {
        session()->flash('location', "questions");
        if($this->authorize('update', $answer)){
            return view('answers.edit', compact(['question', 'answer']));
        }
        return abort(403);
    }

    public function update(Request $request, Question $question, Answer $answer) {
        if($this->authorize('update', $answer)){
            // dd($request, $answer);

            $pattern = '/<a\shref=".*?">@(.*?)<\/a>/';
            $matches = [];
            preg_match_all($pattern, $request->body, $matches);

            $users = $matches[1];
            $newMentions = [];
            foreach ($users as $user) {
                $pattern = "/$user/";
                if(!preg_match($pattern, $answer->body)){
                    array_push($newMentions, $user);
                }
            }

            $answer->update([
                'body' => $request->body,
                'modified_at' => Carbon::now()
            ]);

            foreach($newMentions as $user) {
                if(User::where('name', 'LIKE', $user)->count() > 0){
                    $mentionedUser = User::where('name', 'LIKE', $user)->get()[0];
                    $mentionedUser->notify(new UserMentionedInAnswer($answer));
                }
            }

            session()->flash('success', "Answer has been updated!");
            session()->flash('location', "questions");
            return redirect($question->url);
        }
        return abort(403);
    }

    public function destroy(Question $question, Answer $answer){
        if($this->authorize('delete', [$answer, $question])){
            $answer->delete();

            session()->flash('success', "Answer has been deleted!");
            session()->flash('location', "questions");
            return redirect($question->url);
        }
        return abort(403);
    }
}
