<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Notifications\NewVoteRecieved;
use Illuminate\Http\Request;

class VotesController extends Controller
{
    private function canUpdateVote(Question|Answer $target, int $vote){
        return (
            ($vote === -1 && $target->hasMarkedUpvote(auth()->user()))
                                      ||
            ($vote === 1 && $target->hasMarkedDownvote(auth()->user()))
        );
    }

    public function voteQuestion(Question $question, int $vote) {
        if($question->hasVoted(auth()->user())) {
            if($this->canUpdateVote($question, $vote)){
                $question->updateVote($vote);
            }   else    {
                $question->deleteVote($vote);
            }
        }   else    {
            $question->owner->notify(new NewVoteRecieved($question, auth()->user()));
            $question->vote($vote);
        }
        return redirect()->back();
    }

    public function voteAnswer(Answer $answer, int $vote) {
        if($answer->hasVoted(auth()->user())) {
            if($this->canUpdateVote($answer, $vote)){
                $answer->updateVote($vote);
            }   else    {
                $answer->deleteVote($vote);
            }
        }   else    {
            $answer->author->notify(new NewVoteRecieved($answer, auth()->user()));
            $answer->vote($vote);
        }
        return redirect()->back();
    }
}
