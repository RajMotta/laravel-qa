<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', "Question Answer")</title>
    @yield('page-level-styles')
    @include('frontend-layouts.partials.styles')
  </head>
  <body>
    <div style="height: 100vh">
        @include('frontend-layouts.partials.navbar')
        <div class="row m-0" style="height: 92.5%">
            <div class="col-md-2 bg-dark border-end border-dark" style="--bs-bg-opacity: 0.8">
                @include('frontend-layouts.partials.sidebar')
            </div>
            <div class="col-md-10 bg-dark" style="--bs-bg-opacity: 0.95">
                @include('frontend-layouts.partials.message')
                @yield('content')
            </div>
        </div>
    @yield('page-level-scripts')
    @include('frontend-layouts.partials.scripts')
    </div>
  </body>
</html>
