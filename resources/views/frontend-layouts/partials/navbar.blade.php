<nav class="navbar navbar-expand-lg bg-dark sticky-md-top" z-index="2000" style="height: 7.5%">
    <div class="container">
        <a href="{{ route('questions.index') }}" class="navbar-brand text-light">Stack Overflow</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">

            </ul>
        </div>
        @auth
            <div class="p-2 nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="{{ auth()->user()->avatar }}" class="rounded-circle" width="40px">
                </a>
                <ul class="dropdown-menu justify-content-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="{{ route('users.notifications') }}">Notifications</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{ route('users.profile', auth()->user())}}">Profile</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{ route('questions.create') }}">Ask A Question</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li>
                        <form action="{{ route('logout')}}" method="POST">
                            @csrf
                            <button type="submit" class="bg-transparent border-0 ms-3">Log Out</button>
                        </form>
                    </li>
                </ul>
            </div>
        @else
        <div class="p-2 nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Login Options
            </a>
            <ul class="dropdown-menu justify-content-end" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="{{ route('login') }}">Log In</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="{{ route('register')}} ">Sign Up</a></li>
            </ul>
        </div>
        @endauth
    </div>
</nav>
