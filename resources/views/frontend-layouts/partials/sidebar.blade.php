<ul class="list-group list-group-flush mt-5 text-end">
    <li class="list-group-item  {{ session('location') == "home" ? "bg-secondary" : "bg-transparent" }}">
        <strong>
            <a href="{{ route('users.homepage') }}" class="text-decoration-none {{ session('location') == "home" ? "text-dark" : "text-white-50" }}">
                Home
            </a>
        </strong>
    </li>
    <li class="list-group-item {{ session('location') == "questions" ? "bg-secondary" : "bg-transparent" }}">
        <strong>
            <a href="{{ route('questions.index') }}" class="text-decoration-none {{ session('location') == "questions" ? "text-dark" : "text-white-50" }}">
                Questions
            </a>
        </strong>
    </li>
    <li class="list-group-item {{ session('location') == "tags" ? "bg-secondary" : "bg-transparent" }}">
        <strong>
            <a href="{{ route('tags.index') }}" class="text-decoration-none {{ session('location') == "tags" ? "text-dark" : "text-white-50" }}">
                Tags
            </a>
        </strong>
    </li>
    <li class="list-group-item {{ session('location') == "users" ? "bg-secondary" : "bg-transparent" }}">
        <strong>
            <a href="{{ route('users.index') }}" class="text-decoration-none {{ session('location') == "users" ? "text-dark" : "text-white-50" }}">
                Users
            </a>
        </strong>
    </li>
    <li class="list-group-item {{ session('location') == "ask_question" ? "bg-secondary" : "bg-transparent" }}">
        <strong>
            <a href="{{ route('questions.create') }}" class="text-decoration-none {{ session('location') == "ask_question" ? "text-dark" : "text-white-50" }}">
                Ask A Question
            </a>
        </strong>
    </li>
    <li class="list-group-item {{ session('location') == "notifications" ? "bg-secondary" : "bg-transparent" }}">
        <strong>
            <a href="{{ route('users.notifications') }}" class="text-decoration-none {{ session('location') == "notifications" ? "text-dark" : "text-white-50" }}">
                Notifications
            </a>
        </strong>
    </li>
</ul>
