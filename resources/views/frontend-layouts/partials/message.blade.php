@if(session('success'))
    <div class="alert alert-dismissible alert-success">
        <p><strong>{{ session('success') }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@elseif(session('error'))
    <div class="alert alert-dismissible alert-danger">
        <p><strong>Error: {{ session('error') }}</strong></p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
