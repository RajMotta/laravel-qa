@extends('frontend-layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" />
    <link rel="stylesheet" href="{{ asset('ckeditor4/content.css') }}">
@endsection

@section('content')
<div class="container">
    <div class="col-md-12">
        <div class="row justify-content-center">
            <div class="d-flex flex-column mt-5 list-group list-group-flush">
                <div class="list-group-item bg-transparent">
                    <div class="d-flex justify-content-between mb-4">
                        <h3 class="flex-item text-white">Edit your Answer</h3>
                        <a href="{{ url($answer->url) }}" class="btn btn-dark">Back to Answer</a>
                    </div>
                </div>

                <div class="list-group-item rounded-1 bg-light" style="--bs-bg-opacity: 0.23">
                    <form action="{{ route('questionAnswers.update', [$question, $answer]) }}" method="POST" class="mt-2">
                        @csrf
                        @method("PUT")
                        <div class="form-group mb-3">
                            <label for="body" class="text-white fs-5">Answer</label>
                            {{-- <input type="hidden" class="form-control" value="{{ old('body', $answer->body) }}" name="body" id="body"/>
                            <trix-editor input="body" placeholder="Answer The Question" class="form-control"></trix-editor> --}}

                            <textarea name="body" id="body">{{ old('body', $answer->body) }}</textarea>

                            @error('body')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <input type="submit" value="Submit Answer" class="btn btn-dark">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <script src="{{ asset('js/ckeditorMentions.js') }}"></script>
@endsection
