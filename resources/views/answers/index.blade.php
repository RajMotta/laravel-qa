<div class="row mt-5">
    <div class="col-md-12">
        <div class="d-flex flex-column list-group list-group-flush">
            <div class="list-group-item bg-transparent">
                <div class="d-flex justify-content-between mb-4">
                    <h3 class="flex-item text-white">{{ $question->answers_count }} {{ Str::plural('Answer', $question->answers_count) }}</h3>
                </div>
            </div>
        </div>
        <div id="answerList" class="list-group-item bg-transparent mt-4">
            @foreach ($question->answers_with_favourite as $answer)
                <div id="{{ "$question->id-$answer->id" }}" class="row align-items-center mb-2">
                    <div class="d-flex flex-column align-items-center col-md-1">
                        @auth
                            <div class="mb-3">
                                <form action="{{ route('answers.vote', [$answer, 1]) }}" method="POST">
                                    @csrf
                                    <button type="submit" width="100%" title="Vote Up" class="vote-up border-0 bg-transparent {{ $answer->hasMarkedUpvote(auth()->user()) ? "text-success" : "text-white" }}">
                                        <i class="fa-solid fa-caret-up fa-3x"></i>
                                    </button>
                                </form>
                                <h4 class="m-0 text-muted text-center">{{ $answer->votes_count }}</h4>
                                <form action="{{ route('answers.vote', [$answer, -1]) }}" method="POST">
                                    @csrf
                                    <button type="submit" title="Vote Down" class="vote-down border-0 bg-transparent {{ $answer->hasMarkedDownvote(auth()->user()) ? "text-danger" : "text-white" }}">
                                        <i class="fa fa-caret-down fa-3x"></i>
                                    </button>
                                </form>
                            </div>
                        @else
                            <div class="mb-2">
                                <form action="{{ route('login') }}" method="GET">
                                    @csrf
                                    <button type="submit" title="Vote Up" class="vote-up text-center border-0 bg-transparent text-white">
                                        <i class="fa fa-caret-up fa-3x"></i>
                                    </button>
                                </form>
                                <h4 class="m-0 text-muted text-center">{{ $answer->votes_count }}</h4>
                                <form action="{{ route('login') }}" method="GET">
                                    @csrf
                                    <button type="submit" title="Vote Down" class="vote-down text-center border-0 bg-transparent text-white">
                                        <i class="fa fa-caret-down fa-3x"></i>
                                    </button>
                                </form>
                            </div>
                        @endauth
                        <div class="mt-3">
                            @can('markAsBest', $answer)
                                <form action="{{route($question->best_answer_id == $answer->id ? 'unMarkAsBest':'markAsBest', ['question' => $question, 'answer'=>$answer])}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button title="Mark as Best"
                                            class="favorite d-block text-center mb-2 border-0 bg-transparent"
                                            type="submit">
                                        <i class="fa fa-check fa-2x p-2 {{ $answer->isBest($question) ? 'text-dark bg-success rounded-2' : 'text-white' }}"></i>
                                    </button>
                                </form>
                            @else
                                @if($answer->isBest($question))
                                    <i class="fa fa-check fa-2x text-dark bg-success rounded-2 p-2"></i>
                                @endif
                            @endcan
                        </div>
                    </div>
                    <div class="col-md-10 text-white align-self-start mt-2">
                        <div class="answer-body mb-5">
                            {!! $answer->body !!}
                        </div>
                        <div class="submitter-details fs-6 text-white-50 me-5">
                            Answered By:
                            <a href="{{ route('users.profile', $answer->author) }}" class="text-primary text-decoration-none fs-6 fw-light ms-2">
                                <img src="{{ $answer->author->avatar}}" height="25px" class="rounded-circle">
                                {{ $answer->author->name }}
                            </a>
                        </div>
                        <div class="submission-details fs-6 text-white-50">
                            @if($answer->is_modified)
                                Modified: <p class="fw-light d-inline">{{ $answer->modified_date }}.</p>
                            @else
                                Answered: <p class="fw-light d-inline">{{ $answer->created_date }}.</p>
                            @endif
                        </div>
                    </div>
                    <div class="d-flex col-md-1 justify-content-end align-self-end">
                        @can('update', $answer)
                            <a href="{{ route('questionAnswers.edit', [$question, $answer]) }}" class="btn btn-sm btn-secondary text-warning me-2"><strong>Edit</strong></a>
                        @endcan
                        @can('delete', [$answer, $question])
                            <form action="{{ route('questionAnswers.delete', [$question, $answer]) }}" class="d-inline" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-secondary" style="color: red"><strong>Delete</strong></button>
                            </form>
                        @endcan
                    </div>
                    </div>
                    <hr class="text-white-50 mt-4">
                @endforeach
            </div>
        </div>
    </div>
</div>
