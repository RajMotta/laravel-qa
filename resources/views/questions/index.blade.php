@extends('frontend-layouts.app')

@section('page-level-styles')
    <style>
        .answers {
            padding: 2px;
            border-radius: 5px;
        }

        .answers.answered {
            border: solid 1px #4fc627;
            color: #4fc627;
        }

        .answers.has-best-answer {
            border: solid 1px #4fc627;
            background: #4fc627;
            color: white;
        }

        .answers.unanswered {
            border: solid 1px red;
            color: red;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="d-flex flex-column mt-5 list-group list-group-flush">
            <div class="list-group-item bg-transparent">
                <div class="d-flex justify-content-between mb-4">
                    <h3 class="flex-item text-white">All Questions</h3>
                    <a href="{{ route('questions.create') }}" class="btn btn-dark">Ask a Question!</a>
                </div>
            </div>
            <div id="questionList" class="list-group-item bg-transparent mt-4">
                @foreach ($questions as $question)
                    @include('questions.partials._question')
                @endforeach
            </div>
            <div class="questionListFooter list-group-item bg-transparent mt-2 mb-5">
                {{ $questions->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
