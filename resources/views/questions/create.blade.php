@extends('frontend-layouts.app')

@section('page-level-styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" />
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex flex-column mt-5 list-group list-group-flush">
                <div class="list-group-item bg-transparent">
                    <div class="d-flex justify-content-between mb-4">
                        <h3 class="flex-item text-white">Ask a Question</h3>
                        <a href="{{ route('questions.index') }}" class="btn btn-dark">Back to Home</a>
                    </div>
                </div>
                <div class="list-group-item rounded-1 bg-light" style="--bs-bg-opacity: 0.23">
                    <form action="{{ route('questions.store') }}" method="POST" class="mt-2">
                        @csrf
                        <div class="form-group mb-3">
                            <label for="title" class="text-white fs-5">Title</label>
                            <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" value="{{ old('title') }}" placeholder="Enter Question Title" name="title" id="title"/>

                            @error('title')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="body" class="text-white fs-5">Question</label>
                            <input type="hidden" class="form-control" value="{{ old('body') }}" name="body" id="body"/>
                            <trix-editor input="body" placeholder="Ask Your Question" class="form-control"></trix-editor>

                            @error('body')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group mb-3 d-flex flex-column">
                            <label for="tags" class="text-white fs-5">Select Relevant Tags</label>
                            <select name="tags[]" id="tags" class="form-control select2" multiple>
                                @foreach ($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>

                            @error('tags[]')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>


                        <div class="form-group mb-3">
                            <input type="submit" value="Ask Experts" class="btn btn-dark">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        console.log("running");
        $('.select2').select2({
            placeholder: 'Select an option',
            allowClear: true
        });

        </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
@endsection
