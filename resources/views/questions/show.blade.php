@extends('frontend-layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex flex-column mt-5 list-group list-group-flush">
                <div id="questionTitle" class="list-group-item bg-transparent">
                    <div class="d-flex justify-content-between mb-2">
                        <h3 class="flex-item text-white fw-normal">{{ $question->title }}</h3>
                        <div>
                            <a href="{{ url("$question->url/download") }}" class="btn btn-dark text-success">Download Conversation!</a>
                            <a href="{{ route('questions.index') }}" class="btn btn-dark">Back to Home</a>
                        </div>
                    </div>
                    <div class="row text-white-50 mb-2 d-flex align-items-center">
                        @if($question->is_modified)
                            <div class="col-md-2">
                                <small>
                                    Modified: <p class="fw-light d-inline">{{ $question->modified_date }}.</p>
                                </small>
                            </div>
                        @else
                            <div class="col-md-2">
                                <small>
                                    Asked: <p class="fw-light d-inline">{{ $question->created_date }}.</p>
                                </small>
                            </div>
                        @endif
                        <div class="col-md-2">
                            <small>
                                Viewed: {{ $question->views_count }} times
                            </small>
                        </div>
                        <div class="col-md-2">
                            <small>
                                Asked By:
                                <a href="{{ route('users.profile', $question->owner) }}" class="text-white text-decoration-none fw-light ms-2">
                                    {{ $question->owner->name }}
                                </a>
                            </small>
                        </div>
                        <div class="col-6 tag-list">
                            @foreach ($question->tags as $tag)
                                <a href="" class="btn bg-primary btn-sm ms-2 fs-6 text-info text-decoration-none" style="--bs-bg-opacity:0.5;">
                                    <small>{{ $tag->name }}</small>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div id="questionBody" class="list-group-item bg-transparent text-white">
                    <div class="row mb-5">
                        <div class="col-md-1 d-flex flex-column align-items-center">
                            @auth
                                <div id="votingPanelLoggedIn" class="mb-3">
                                    <form action="{{ route('questions.vote', [$question, 1]) }}" method="POST">
                                        @csrf
                                        <button type="submit" width="100%" title="Vote Up" class="vote-up border-0 bg-transparent {{ $question->hasMarkedUpvote(auth()->user()) ? "text-success" : "text-white" }}">
                                            <i class="fa-solid fa-caret-up fa-3x"></i>
                                        </button>
                                    </form>
                                    <h4 class="m-0 text-muted text-center">{{ $question->votes_count }}</h4>
                                    <form action="{{ route('questions.vote', [$question, -1]) }}" method="POST">
                                        @csrf
                                        <button type="submit" title="Vote Down" class="vote-down border-0 bg-transparent {{ $question->hasMarkedDownvote(auth()->user()) ? "text-danger" : "text-white" }}">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </button>
                                    </form>
                                </div>
                            @else
                                <div id="votingPanelLoggedOut" class="mb-3">
                                    <form action="{{ route('login') }}" method="GET">
                                        @csrf
                                        <button type="submit" title="Vote Up" class="vote-up text-center border-0 bg-transparent text-white">
                                            <i class="fa fa-caret-up fa-3x"></i>
                                        </button>
                                    </form>
                                    <h4 class="m-0 text-muted text-center">{{ $question->votes_count }}</h4>
                                    <form action="{{ route('login') }}" method="GET">
                                        @csrf
                                        <button type="submit" title="Vote Down" class="vote-down text-center border-0 bg-transparent text-white">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </button>
                                    </form>
                                </div>
                            @endauth
                            @can('markAsFavourite', $question)
                                <div id="markAsFavouritePanel" class="mb-3">
                                    @if($question->is_favourite)
                                        <form action="{{ route('questions.unfavourite', $question) }}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" title="Unmark Favourite" class="favourite d-block text-center mb-2 border-0 bg-transparent">
                                                <i class="fa fa-star fa-2x text-warning"></i>
                                            </button>
                                        </form>
                                        <h4 class="text-warning text-center">{{ $question->favourites_count}}</h4>
                                    @else
                                        <form action="{{ route('questions.favourite', $question) }}" method="POST">
                                            @csrf
                                            <button type="submit" title="Mark as Favourite" class="favourite d-block text-center mb-2 border-0 bg-transparent">
                                                <i class="fa fa-star fa-2x text-white"></i>
                                            </button>
                                        </form>
                                        <h4 class="text-white text-center">{{ $question->favourites_count}}</h4>
                                    @endif
                                </div>
                            @endcan
                            <div id="editDeletePanel" class="mb-3">
                                @can('update', $question)
                                    <a href="{{ route('questions.edit', $question) }}" class="btn btn-sm btn-secondary text-warning mb-2 d-block"><strong>Edit</strong></a>
                                @endcan
                                @can('delete', $question)
                                    <form action="{{ route('questions.destroy', $question) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-secondary" style="color: red"><strong>Delete</strong></button>
                                    </form>
                                @endcan
                            </div>
                        </div>
                        <div class="col-md-11 d-flex flex-column justify-content-between">
                            <div class="questionBodyPanel">
                                {!! $question->body !!}
                            </div>
                            <div class="giveAnswerPanel">
                                <div class="text-end mt-4">
                                    <a href="{{ route('questionAnswers.create', $question) }}" class="btn btn-primary">
                                        Give an Answer!
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="answerSection bg-transparent text-white" id="list-group-item">
                    @include('answers.index');
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
