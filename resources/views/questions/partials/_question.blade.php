<div class="row align-items-center mb-2">
    <div class="col-md-1 text-white-50 views">
        {{ $question->views_count }} {{ Str::plural('view', $question->views_count) }}
    </div>
    <div class="col-md-10">
        <a href="{{ $question->url }}" class="text-primary text-decoration-none fs-5">{{ $question->title }}</a>
    </div>
    <div class="d-flex col-md-1 justify-content-end">
        @can('update', $question)
            <a href="{{ route('questions.edit', $question) }}" class="btn btn-sm btn-secondary text-warning me-2"><strong>Edit</strong></a>
        @endcan
        @can('delete', $question)
            <form action="{{ route('questions.destroy', $question) }}" class="d-inline" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-sm btn-secondary" style="color: red"><strong>Delete</strong></button>
            </form>
        @endcan
    </div>
</div>
<div class="row align-items-center mb-2">
    <div class="col-md-1 answers {{ $question->styles_for_answer }}">
        {{ $question->answers_count }} {{ Str::plural('answer', $question->answers_count) }}
    </div>
    <div class="col-md-11 fs-6 text-white-50 fw-light">
            {!! \Illuminate\Support\Str::limit($question->body, 250) !!}
    </div>
</div>
<div class="row align-items-center mb-2">
    <div class="col-md-1 text-white-50 {{ $question->styles_for_answer }}">
        {{ $question->votes_count }} {{ Str::plural('vote', $question->votes_count) }}
    </div>
    <div class="col-md-11 row">
        <span class="col-4 submitter-details fs-6 text-white-50">
            Submitted By:
            <a href="{{ route('users.profile', $question->owner) }}" class="text-primary text-decoration-none fs-6 fw-light ms-2">
                <img src="{{ $question->owner->avatar}}" height="25px" class="rounded-circle">
                {{ $question->owner->name }}
            </a>
        </span>
        <span class="col-3 submission-details fs-6 text-white-50">
            @if($question->is_modified)
                Modified: <p class="fw-light d-inline">{{ $question->modified_date }}.</p>
            @else
                Submitted: <p class="fw-light d-inline">{{ $question->created_date }}.</p>
            @endif
        </span>
        <span class="col-5 tag-list fs-5 text-white-50">
            @foreach ($question->tags as $tag)
                <a href="{{ route('tags.show', $tag) }}" class="btn bg-primary btn-sm me-2 fs-6 text-info text-decoration-none" style="--bs-bg-opacity:0.5;">
                    <small>{{ $tag->name }}</small>
                </a>
            @endforeach
        </span>
    </div>
</div>
<hr class="text-white">
