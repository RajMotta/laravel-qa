@extends('frontend-layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="d-flex flex-column mt-5 list-group list-group-flush">
            <div class="list-group-item bg-transparent">
                <div class="d-flex justify-content-between mb-4">
                    <h3 class="flex-item text-white">Tags</h3>
                    <a href="{{ route('questions.index') }}" class="btn btn-dark">Back To Questions</a>
                </div>
            </div>
            <div class="list-group-item bg-transparent d-flex flex-wrap">
                @foreach ($tags as $tag)
                    <div class="col-md-4 p-3">
                        <div class="tagHeader row d-flex align-items-center mb-2">
                            <div class="col-md-4 text-primary">
                                <a href="{{ route('tags.show', $tag) }}" class="fs-4 text-decoration-none">{{$tag->name}}</a>
                            </div>
                            <div class="col-md-8">
                                <p class="text-white fs-5 m-0">
                                    Questions Asked: {{ $tag->questions()->count() }}
                                </p>
                            </div>
                        </div>
                        <div class="tagDescription row">
                            <div class="col-md-12">
                                <div class="mt-2 mb-2 fs-6 p-2 bg-dark rounded-1 text-white-50">
                                    {{$tag->description}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="questionListFooter list-group-item bg-transparent mt-2 mb-5">
                {{ $tags->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
