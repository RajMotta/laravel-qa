@extends('frontend-layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="d-flex flex-column mt-5 list-group list-group-flush">
            <div class="list-group-item bg-transparent">
                <div id="profileTitle" class="d-flex justify-content-between text-white">
                    <h3 class="flex-item text-white m-0"><img src="{{ $user->avatar }}" class="rounded-circle" width="40px"> {{ $user->name }}</h3>
                    <div id="user-actions">
                        @can('viewFavourites', $user)
                            <a href="{{ route('users.viewFavourites', $user) }}" class="btn btn-dark text-success align-self-center fw-bold">View Favourites</a>
                        @endcan
                        @can('update', $user)
                            <a href="{{ route('users.edit', $user) }}" class="btn btn-dark text-warning align-self-center fw-bold">Edit Profile</a>
                        @endcan
                    </div>
                </div>
                <div id="profileHeader" class="d-flex justify-content-start mb-4 text-white-50 ms-5">
                    <small id="joinDate">
                        Joined: {{ $user->created_date }}
                    </small>
                    <small id="lastActive" class="ms-5">
                        Last Active: 10 minutes ago
                    </small>
                </div>
            </div>
            <div class="list-group-item bg-transparent text-white mt-5">
                <div class="row">
                    <div class="col-md-4">
                        <div id="statistics">
                            <h4 class="ms-4">
                                Stats
                            </h4>
                            <div class="bg-dark ms-4 mt-3 me-4 p-3 rounded-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        Questions Asked: {{ $user->questions->count() }}
                                    </div>
                                    <div class="col-md-6">
                                        Answers Given: {{ $user->answers->count() }}
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        Question Votes: {{ $votes['questions'] }}
                                    </div>
                                    <div class="col-md-6">
                                        Answer Votes: {{ $votes['answers'] }}
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        Views: {{ $views }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="tag-details" class="mt-4">
                            <h4 class="ms-4">
                                Tags Used
                            </h4>
                            <div class="bg-dark ms-4 mt-3 me-4 p-3 rounded-2">
                                @foreach ($tags as $tag)
                                    <div class="row mt-1">
                                        <div class=" tag-name col-6">
                                            <a href="{{ route('tags.show', $tag->id)}}" class="text-decoration-none fw-bold text-light">{{ $tag->name }}</a>
                                        </div>
                                        <div class="tag-count col-6">{{ $tag->count }}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div id="userAbout">
                            <h4>About</h4>
                            @if($user->bio !== null)
                                <div class="bg-dark mt-3 rounded-2 p-3">
                                    {!! $user->bio !!}
                                </div>
                            @endif
                        </div>
                        <div id="userPosts" class="{{$user->bio !== "" ? "mt-5" : "mt-3"}}">
                            <ul class="nav nav-tabs border-0" id="postsTabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active text-decoration-none border-0 fw-bold fs-5" id="questions-tab" data-bs-toggle="tab" data-bs-target="#questions-tab-pane" type="button" role="tab" aria-controls="questions-tab-pane" aria-selected="true"
                                    style="
                                        --bs-nav-link-color: #f8f9fa; --bs-nav-tabs-link-active-color: #f8f9fa;
                                        --bs-nav-tabs-link-active-bg: #212529;
                                        --bs-nav-tabs-link-hover-border-color: #6c757d; --bs-nav-tabs-link-active-border-color: #212529;
                                    ">
                                        Questions
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link text-decoration-none border-0 fw-bold fs-5" id="answers-tab" data-bs-toggle="tab" data-bs-target="#answers-tab-pane" type="button" role="tab" aria-controls="answers-tab-pane" aria-selected="false"
                                    style="
                                        --bs-nav-link-color: #f8f9fa; --bs-nav-tabs-link-active-color: #f8f9fa;
                                        --bs-nav-tabs-link-active-bg: #212529;
                                        --bs-nav-tabs-link-active-border-color: #212529;
                                    ">
                                        Answers
                                    </button>
                                </li>
                            </ul>
                            <div class="tab-content p-3 bg-dark" id="myTabContent">
                                <div class="tab-pane fade show active" id="questions-tab-pane" role="tabpanel" aria-labelledby="questions-tab" tabindex="0">
                                    @foreach ($user->questions as $question)
                                        <div class="row align-items-center mb-2">
                                                <a href="{{ url($question->url) }}" class="text-primary text-decoration-none fs-5">{{ $question->title }}</a>
                                        </div>
                                        <div class="row align-items-center mb-2">
                                            <div class="col-md-11 fs-6 text-white-50 fw-light">
                                                    {!! \Illuminate\Support\Str::limit($question->body, 125) !!}
                                            </div>
                                        </div>
                                        <hr class="text-white">
                                    @endforeach
                                </div>
                                <div class="tab-pane fade" id="answers-tab-pane" role="tabpanel" aria-labelledby="answers-tab" tabindex="0">
                                    @foreach ($user->answers as $answer)
                                        <div class="row align-items-center mb-2">
                                            <div class="col-md-10">
                                                <a href="{{ url($answer->url) }}" class="text-primary text-decoration-none fs-5">{{ $answer->question->title }}</a>
                                            </div>
                                        </div>
                                        <div class="row align-items-center mb-2">
                                            <div class="col-md-11 fs-6 text-white-50 fw-light">
                                                    {!! \Illuminate\Support\Str::limit($answer->body, 125) !!}
                                            </div>
                                        </div>
                                        <hr class="text-white">
                                    @endforeach
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

