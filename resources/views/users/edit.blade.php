@extends('frontend-layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" />
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex flex-column mt-5 list-group list-group-flush">
                <div class="list-group-item bg-transparent">
                    <div class="d-flex justify-content-between mb-4">
                        <h3 class="flex-item text-white">Edit Your Account Details</h3>
                        <a href="{{ route('users.profile', $user) }}" class="btn btn-dark">Back to Profile</a>
                    </div>
                </div>
                <div class="list-group-item rounded-1 bg-light" style="--bs-bg-opacity: 0.23">
                    <form action="{{ route('users.update', $user) }}" method="POST" class="mt-2" enctype="multipart/form-data">
                        @csrf
                        @method("PUT")
                        <div class="form-group mb-3">
                            <label for="name" class="form-label text-white fs-5">Username</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name', $user->name) }}" placeholder="Enter Username" name="name" id="name"/>

                            @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="bio" class="form-label text-white fs-5">Bio/Description</label>
                            <input type="hidden" class="form-control" value="{{ old('bio', $user->bio) }}" name="bio" id="bio"/>
                            <trix-editor input="bio" placeholder="Your bio" class="form-control"></trix-editor>

                            @error('bio')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="image" class="form-label text-white fs-5">Upload Profile Image</label>
                            <div class="row">
                                <div class="col-2">
                                    <img id="temp-image" class="rounded-circle" src="{{ $user->avatar }}" width="100%">
                                </div>
                                <div class="col-10">
                                    <input class="form-control" type="file" id="image" name="image" value="{{ $user->avatar }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <input type="submit" value="Update Profile" class="btn btn-dark">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
    <script>
        const image = document.getElementById("image");
        image.addEventListener('change', displayImage);
        function displayImage(evt) {
            var reader = new FileReader();
            reader.readAsDataURL(evt.target.files[0]);
            reader.onload = (evt) => {
                document.getElementById('temp-image').setAttribute('src', evt.target.result);
            }
        }
    </script>
@endsection
