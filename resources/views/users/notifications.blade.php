@extends('frontend-layouts.app')

@section('content')
    <?php
        function getAnswerUrl($answerObj) {
            $answer = App\Models\Answer::find($answerObj['id']);
            if($answer){
                return url($answer->url);
            }   else {
                return route('questions.index');
            }

        }

        function getQuestionUrl($questionObj) {
            $question = App\Models\Question::find($questionObj['id']);
            if($question){
                return url($question->url);
            }   else {
                return route('questions.index');
            }
        }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-column mt-5 list-group list-group-flush">
                    <div class="list-group-item bg-transparent">
                        <div class="d-flex justify-content-between mb-4">
                            <h3 class="flex-item text-white">Notifications</h3>
                            <a href="{{ route('questions.index') }}" class="btn btn-dark">Back to Home</a>
                        </div>
                    </div>
                    <div id="notificationList" class="list-group-item bg-transparent mt-4">
                        <form action="{{ route('users.markAllNotificationsAsRead') }}" method="POST">
                            @csrf
                            @method("PUT")
                            <button type="submit" class="btn btn-sm btn-primary mb-3">Mark All as Read</button>
                        </form>
                        <ul class="list-group list-group-flush">
                            @forelse ($notifications as $notification)
                                <li class="list-group-item bg-dark d-flex justify-content-between align-items-center text-white" style="--bs-bg-opacity: 0.4">
                                    <p>
                                    @if($notification->type === App\Notifications\NewReplyAdded::class)
                                            User: {{ $notification->data['answer']['author']['name'] }} has Responded to your
                                            <a href="{{ getAnswerUrl($notification->data['answer']) }}">Question</a>
                                    @elseif ($notification->type === App\Notifications\NewVoteRecieved::class)
                                        User: {{ $notification->data['user']['name'] }} Voted your
                                            @if($notification->data['target_type'] === App\Models\Question::class)
                                            <a href="{{ getQuestionUrl($notification->data['target']) }}">Question</a>
                                            @elseif ($notification->data['target_type'] === App\Models\Answer::class)
                                                <a href="{{ getAnswerUrl($notification->data['target']) }}">Answer</a>
                                            @endif
                                    @elseif ($notification->type === App\Notifications\AnswerMarkedAsBest::class)
                                            User: {{ $notification->data['answer']['question']['owner']['name'] }} has marked your
                                            <a href="{{ getAnswerUrl($notification->data['answer']) }}"> Answer</a>
                                            As best!
                                    @elseif ($notification->type === App\Notifications\QuestionMarkedAsFavourite::class)
                                            User: {{ $notification->data['user']['name']}} has marked your
                                            <a href="{{ getQuestionUrl($notification->data['question']) }}"> Question</a>
                                            As their Favourite!
                                    @elseif ($notification->type === App\Notifications\UserMentionedInAnswer::class)
                                            User: <a href="{{"users/{$notification->data['user']['id']}/profile"}}">{{ $notification->data['user']['name'] }}</a> has mentioned you in their
                                            <a href="{{ getAnswerUrl($notification->data['answer']) }}"> Answer</a>
                                    @endif
                                    </p>
                                    <form action="{{ route('users.markNotificationAsRead', $notification)}}" method="POST">
                                        @csrf
                                        @method("PUT")
                                        <button class="btn btn-sm btn-primary">Mark As Read</button>
                                    </form>
                                </li>
                            @empty
                                <h5 class="text-white">No Notifications to Check! <a href="{{ route('questions.index') }}">Back to Home!</a></h5>
                            @endforelse
                        </ul>
                    </div>
                    <div class="list-group-item bg-transparent">
                        {{ $notifications->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
