@extends('frontend-layouts.app')


@section('page-level-styles')
    <style>
        .answers {
            padding: 2px;
            border-radius: 5px;
        }

        .answers.answered {
            border: solid 1px #4fc627;
            color: #4fc627;
        }

        .answers.has-best-answer {
            border: solid 1px #4fc627;
            background: #4fc627;
            color: white;
        }

        .answers.unanswered {
            border: solid 1px red;
            color: red;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="d-flex flex-column mt-5 list-group list-group-flush">
            <div class="list-group-item bg-transparent">
                <div class="d-flex justify-content-between mb-4">
                    <h3 class="flex-item text-white">Favourite Questions</h3>
                    <div id="action-buttons">
                        <a href="{{ route('users.profile', $user) }}" class="btn btn-dark me-3">Back To Profile</a>
                        <a href="{{ route('questions.create') }}" class="btn btn-dark">Ask a Question!</a>
                    </div>
                </div>
            </div>
            <div id="questionList" class="list-group-item bg-transparent mt-4">
                @foreach ($questions as $question)
                    <div class="row align-items-center mb-2">
                        <div class="col-md-1 text-white-50 views">
                            {{ $question->views_count }} {{ Str::plural('view', $question->views_count) }}
                        </div>
                        <div class="col-md-10">
                            <a href="{{ url($question->url) }}" class="text-primary text-decoration-none fs-5">{{ $question->title }}</a>
                        </div>
                        <div class="d-flex col-md-1 justify-content-end">
                            @can('update', $question)
                                <a href="{{ route('questions.edit', $question) }}" class="btn btn-sm btn-secondary text-warning me-2"><strong>Edit</strong></a>
                            @endcan
                            @can('delete', $question)
                                <form action="{{ route('questions.destroy', $question) }}" class="d-inline" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-secondary" style="color: red"><strong>Delete</strong></button>
                                </form>
                            @endcan
                        </div>
                    </div>
                    <div class="row align-items-center mb-2">
                        <div class="col-md-1 answers {{ $question->styles_for_answer }}">
                            {{ $question->answers_count }} {{ Str::plural('answer', $question->answers_count) }}
                        </div>
                        <div class="col-md-11 fs-6 text-white-50 fw-light">
                                {!! \Illuminate\Support\Str::limit($question->body, 250) !!}
                        </div>
                    </div>
                    <div class="row align-items-center mb-2">
                        <div class="col-md-1 text-white-50 {{ $question->styles_for_answer }}">
                            {{ $question->votes_count }} {{ Str::plural('vote', $question->votes_count) }}
                        </div>
                        <div class="col-md-11">
                            <span class="submitter-details fs-6 text-white-50 me-5">
                                Submitted By:
                                <a href="{{ route('users.profile', $question->owner) }}" class="text-primary text-decoration-none fs-6 fw-light ms-2">
                                    <img src="{{ $question->owner->avatar}}" height="25px">
                                    {{ $question->owner->name }}
                                </a>
                            </span>
                            <span class="submission-details fs-6 text-white-50">
                                @if($question->is_modified)
                                    Modified: <p class="fw-light d-inline">{{ $question->modified_date }}.</p>
                                @else
                                    Submitted: <p class="fw-light d-inline">{{ $question->created_date }}.</p>
                                @endif
                            </span>
                        </div>
                    </div>
                    <hr class="text-white">
                @endforeach
            </div>
            <div class="questionListFooter list-group-item bg-transparent mt-2 mb-5">
                {{ $questions->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
