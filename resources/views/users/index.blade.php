@extends('frontend-layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="d-flex flex-column mt-5 list-group list-group-flush">
            <div class="list-group-item bg-transparent">
                <div class="d-flex justify-content-between mb-4">
                    <h3 class="flex-item text-white">Users</h3>
                    <a href="{{ route('questions.index') }}" class="btn btn-dark">Back To Questions</a>
                </div>
            </div>
            <div class="list-group-item bg-transparent d-flex flex-wrap">
                @foreach ($users as $user)
                    <div class="col-md-4 p-3">
                        <div class="tagHeader row d-flex align-items-center mb-2">
                            <div class="col-md-2">
                                <img src="{{ $user->avatar }}">
                            </div>
                            <div class="col-md-5 text-primary">
                                <a href="{{ route('users.profile', $user) }}" class="fs-6 text-decoration-none">{{ $user->name }}</a>
                            </div>
                            <div class="col-md-5">
                                <p class="text-white fs-6 m-0">
                                    Questions Asked: {{ $user->questions->count() }}
                                </p>
                                <p class="text-white fs-6 m-0">
                                    Answers Given: {{ $user->answers->count()}}
                                </p>
                            </div>
                        </div>
                        @if($user->bio !== null)
                            <div class="userBio row">
                                <div class="col-md-12">
                                    <div class="mt-2 mb-2 fs-6 p-2 bg-dark rounded-1 text-white-50">
                                        {!! $user->bio !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
            <div class="questionListFooter list-group-item bg-transparent mt-2 mb-5">
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
