@extends('frontend-layouts.app')

@section('content')
<div class="d-flex flex-column align-items-center justify-content-center" style="height:100%">
    <h1 class="text-white text-center mb-4">
        Stack Overflow
    </h1>
    <div class="text-white-50 text-center w-50 mb-4">
        <p class="fs-5">
            A public platform building the definitive collection of coding questions & answers
        </p>
        <p class="fs-5">
            A community-based space to find and contribute answers to technical challenges, and one of the most popular websites in the world.
        </p>

    </div>
    <div class="text-white d-flex align-items-center">
        <a href="{{route('login')}} " class="btn btn-light btn-lg me-5">Login</a>
        <p class="fs-4 mt-2 me-5">or</p>
        <a href="{{route('register')}} " class="btn btn-primary btn-lg me-3">Sign-Up</a>
    </div>
</div>
@endsection
