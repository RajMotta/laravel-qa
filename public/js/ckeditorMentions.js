'use strict';

	( async function() {

        var response = await fetch("http://127.0.0.1:8000/api/users/fetchAll");
        let output = await response.json();
        let data = output.data;
        console.log(data);

		CKEDITOR.replace( 'body', {
			height: 240,
			extraPlugins: 'mentions,easyimage,sourcearea,toolbar,undo,wysiwygarea,basicstyles',
			extraAllowedContent: 'h1',
			mentions: [

				{
					feed: CKEDITOR.tools.array.map( data, function( item ) {
									return item.name;
								} ),
					minChars: 0,
                    outputTemplate: '<a href="http://127.0.0.1:8000/users/{id}/profile">{name}</a>'

				},
				{
					feed: '{encodedQuery}',
					marker: '$',
					minChars: 1,
					template: '<li data-id="{id}">{fullName}</li>'
				},
				{
					feed: dataCallback,
					marker: '#',
					template: '<li data-id="{id}">{name} ({fullName})</li>'
				}
			],
			on: {
				instanceReady: function() {
					// We have to stub ajax.load function.
					CKEDITOR.ajax.load = function( query, callback ) {
						var results = data.filter( function( item ) {
								return item.name.indexOf( query ) === 0;
							} );

						setTimeout( function() {
							callback( JSON.stringify( results ) );
						}, Math.random() * 500 );
					}
				}
			}
		} );

		function dataCallback( opts, callback ) {
			setTimeout( function() {
				callback(
					data.filter( function( item ) {
						return item.name.indexOf( opts.query ) === 0;
					} )
				);
			}, Math.random() * 500 );
		}

	} )();



