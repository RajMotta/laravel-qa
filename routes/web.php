<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\FavouritesController;
use App\Http\Controllers\QuestionAnswerController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\VotesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/questions/{slug}/download', [QuestionsController::class, 'downloadAsPDF'])->name('questions.download');
Route::resource('/questions', QuestionsController::class)->except('show');

Route::get('/questions/{slug}', [QuestionsController::class, 'show']);

Route::resource('/tags', TagsController::class)->only(['index', 'show']);

Route::put('questions/{question}/answers/{answer}/best-answer', [QuestionAnswerController::class, 'markAsBest'])->name('markAsBest');
Route::put('questions/{question}/answers/{answer}/unmark-best-answer', [QuestionAnswerController::class, 'unMarkAsBest'])->name('unMarkAsBest');

Route::middleware(['auth'])->group(function() {
    Route::get('questions/{question}/answer/create', [QuestionAnswerController::class, 'create'])->name('questionAnswers.create');
    Route::get('question/{question}/answer/{answer}/edit', [QuestionAnswerController::class, 'edit'])->name('questionAnswers.edit');
    Route::put('question/{question}/answer/{answer}/update', [QuestionAnswerController::class, 'update'])->name('questionAnswers.update');
    Route::post('questions/{question}/answer', [QuestionAnswerController::class, 'store'])->name('questionAnswers.store');
});

Route::post('questions/{question}/markAsFavourite', [FavouritesController::class, 'store'])->name('questions.favourite');
Route::delete('questions/{question}/unmarkFavourite', [FavouritesController::class, 'destroy'])->name('questions.unfavourite');

Route::post('questions/{question}/votes/{vote}', [VotesController::class, 'voteQuestion'])->name('questions.vote');
Route::post('answers/{answer}/votes/{vote}', [VotesController::class, 'voteAnswer'])->name('answers.vote');

Route::get('/users/', [UsersController::class, 'index'])->name('users.index');
Route::get('/users/notifications', [UsersController::class, 'notifications'])->name('users.notifications')->middleware(['auth']);
Route::put('/users/notifications/{notification}/markAsRead', [UsersController::class, 'markAsRead'])->name('users.markNotificationAsRead');
Route::put('/users/markAllAsRead', [UsersController::class, 'markAllAsRead'])->name('users.markAllNotificationsAsRead');
Route::get('/users/{user}/profile', [UsersController::class, 'show'])->name('users.profile');
Route::get('/users/{user}/favourites', [UsersController::class, 'viewFavourites'])->name('users.viewFavourites');
Route::get('/users/{user}/edit', [UsersController::class, 'edit'])->name('users.edit');
Route::put('/users/{user}/edit', [UsersController::class, 'update'])->name('users.update');

Route::get('/home-page', [UsersController::class, 'index'])->name('users.homepage');

Route::delete('questions/{question}/answer/{answer}/delete', [QuestionAnswerController::class, 'destroy'])->name('questionAnswers.delete');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
